package io.swagger.petstore.helper;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.File;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.swagger.petstore.tests.BaseTest.URI;

public class Api {
    private String uri;

    public Api(String uri) {
        this.uri = uri;
    }

    public Response get(String uri, Map<String, String> map, String path) {
        return given()
                .baseUri(uri)
                .basePath(path)
                .params(map)
                .contentType(ContentType.JSON)
                .filter(new AllureRestAssured())
                .when()
                .get();
    }

    public Response get(Map<String, String> map, String path) {
        return given()
                .baseUri(uri)
                .basePath(path)
                .params(map)
                .contentType(ContentType.JSON)
                .filter(new AllureRestAssured())
                .when()
                .get();
    }

    public Response post(File fileTemp, String path) {
        return given()
                .multiPart("file", fileTemp)
                .baseUri(uri)
                .contentType(ContentType.MULTIPART)
                .filter(new AllureRestAssured())
                .when()
                .post(path);
    }
    public Response post(String uri, File fileTemp, String path) {
        return given()
                .multiPart("file", fileTemp)
                .baseUri(uri)
                .contentType(ContentType.MULTIPART)
                .filter(new AllureRestAssured())
                .when()
                .post(path);
    }
}
