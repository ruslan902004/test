package io.swagger.petstore.helper;

public class Endpoints {
    public static final String PET_FIND_BY_STATUS = "/v2/pet/findByStatus";
    public static final String PET_UPLOAD_IMAGE = "v2/pet/1/uploadImage";
}
