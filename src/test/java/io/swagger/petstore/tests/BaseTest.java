package io.swagger.petstore.tests;

import io.swagger.petstore.helper.Api;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BaseTest {
    //public static final String URI = "https://petstore.swagger.io";
    public static final String URI;
    public static Api api;
    public static Properties props;

    static {
        props = new Properties();
        InputStream is = ClassLoader.getSystemResourceAsStream("petstore.properties");
        try {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        URI = props.getProperty("uri");
        api = new Api(URI);
    }

    public BaseTest() {
    }
}
