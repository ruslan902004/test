package io.swagger.petstore.tests.pet;

import java.io.File;
import java.net.URISyntaxException;
import java.util.stream.Stream;

import io.swagger.petstore.helper.Api;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static io.swagger.petstore.helper.Endpoints.PET_UPLOAD_IMAGE;
import static io.swagger.petstore.tests.BaseTest.*;

@DisplayName("Pet test")
public class PostPetTests {
    //@Test
    @ParameterizedTest
    @ValueSource(strings = {"v2/pet/1/uploadImage", "v2/pet/12/uploadImage"})
    public void postPet(String inApi) throws URISyntaxException {
        String filePath = getClass().getClassLoader().getResource("PetImage.png").toString();
        File fileTemp = new File(getClass().getClassLoader().getResource("PetImage.png").toURI());
        //File fileTemp =  new File(String.valueOf(new Api(props.getProperty("uri"))));

        String answer = api.post(fileTemp, inApi)//PET_UPLOAD_IMAGE
                .then()
                .statusCode(200)
                .extract()
                .body()
                .asString();
        System.out.println("URL: " + URI);
        System.out.println("POST API: " + inApi);
        System.out.println("POST BODY IMG: " + filePath);
    }

    @ParameterizedTest(name = "Запуск теста с парметром {0}")
    @CsvSource(value = {
            "https://petstore.swagger.io,v2/pet/1/uploadImage",
            "https://petstore.swagger.io,v2/pet1/1/uploadImage"
    })
    public void postPet1(String url, String inApi) throws URISyntaxException {
        String filePath = getClass().getClassLoader().getResource("PetImage.png").toString();
        File fileTemp = new File(getClass().getClassLoader().getResource("PetImage.png").toURI());
        //File fileTemp =  new File(String.valueOf(new Api(props.getProperty("uri"))));

        String answer = api.post(url, fileTemp, inApi)//PET_UPLOAD_IMAGE
                .then()
                .statusCode(200)
                .extract()
                .body()
                .asString();
        System.out.println("URL: " + url);
        System.out.println("POST API: " + inApi);
        System.out.println("POST BODY IMG: " + filePath);
    }

    @ParameterizedTest
    @ArgumentsSource(EmployeesArgumentsProvider.class)
    public void postPet2(Api apiCreated, String inApi) throws URISyntaxException {
        String filePath = getClass().getClassLoader().getResource("PetImage.png").toString();
        File fileTemp = new File(getClass().getClassLoader().getResource("PetImage.png").toURI());
        //File fileTemp =  new File(String.valueOf(new Api(props.getProperty("uri"))));

        String answer = apiCreated.post(fileTemp, inApi)//PET_UPLOAD_IMAGE
                .then()
                .statusCode(200)
                .extract()
                .body()
                .asString();
        System.out.println("URL: " + URI);
        System.out.println("POST API: " + inApi);
        System.out.println("POST BODY IMG: " + filePath);
    }
}

    class EmployeesArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(
                    Arguments.of(new Api("https://petstore.swagger.io"),
                            "v2/pet/1/uploadImage"),
                    Arguments.of(new Api("https://petstore1.swagger.io"),
                            "v2/pet/12/uploadImage")
            );
        }
}
