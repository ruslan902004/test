package io.swagger.petstore.tests.pet;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.swagger.petstore.tests.BaseTest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.swagger.petstore.helper.Endpoints.PET_FIND_BY_STATUS;

public class GetPetTests extends BaseTest{
    @Test
    public void getUrl() {

        Map<String, String> map = new HashMap<>();
        map.put("status", "pending");
        String answer = given()
                .baseUri(URI)
                .basePath("/v2/pet/findByStatus")
                .params(map)
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .statusCode(200)
                .extract()
                .body()
                .asString();
        System.out.println("yo " + answer);
    }


    @Test
    public void getUrl1() {

        Map<String, String> map = new HashMap<>();
        map.put("status", "pending");
        JsonPath answer = given()
                .baseUri("https://petstore.swagger.io")
                .basePath("/v2/pet/findByStatus")
                .params(map)
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .statusCode(200)
                .extract()
                .jsonPath();
        System.out.println(answer.getString("id"));
    }

    @Test
    public void getUrl2() {

        Map<String, String> map = new HashMap<>();
        map.put("status", "pending");
        given()
                .baseUri("https://petstore.swagger.io")
                .basePath("/v2/pet/findByStatus")
                .params(map)
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .statusCode(200)
                .body("id", Matchers.equalTo("12766465"));

    }

    @Test
    public void getUrl4() {

        Map<String, String> map = new HashMap<>();
        map.put("status", "pending");
        String answer = api.get(map, PET_FIND_BY_STATUS)
                .then()
                .statusCode(200)
                .extract()
                .body()
                .asString();
        System.out.println("yo " + answer);
    }
}
